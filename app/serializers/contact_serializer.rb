class ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :birthdate, :author # author is virtual

  belongs_to :kind do
    link(:related) { kind_path(object.kind.id) }
  end

  has_many :phones do
    link(:related) { contact_phones_path(object.kind.id) }
  end

  has_one :address do
    link(:related) { contact_phones_path(object.id) }
  end

  link(:self) { contact_path(object.id) }
  link(:kind) { kind_path(object.kind.id) }

  # def author
  #   "Victor Navarro"
  # end

  def attributes(*args)
    h = super(*args)
    h[:birthdate] = object.birthdate.to_time.iso8601 unless object.birthdate.blank?
    h
  end
end
