class ApplicationController < ActionController::API

    # This should be used to only accept requests with JSON
    # before_filter :endure_json_request

    # def ensure_json_request
    #     return if request.headers["Accept"] =~ /vnd.api+json/
    #     render :nothing => true, :status => 406
    # end

end
