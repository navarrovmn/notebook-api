class AddressesController < ApplicationController
  before_action :set_contact

  # GET /addresses/1
  def show
    render json: @contact.address
  end

  def update
    if @contact.address.update(address_params)
        render json: @contact.address
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
        @contact = Contact.find(params[:contact_id])
    end

    def address_params
       ActiveModelSerializers::Deserialization.jsonapi_parse(params)
    end

end
