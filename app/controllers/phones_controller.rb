class PhonesController < ApplicationController
  before_action :set_phone, only: [:show]

  # GET /phones/1
  def show
    render json: @phones
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phone
        if params[:contact_id]
            @phones = Contact.find(params[:contact_id]).phones
            return @phone
        end

        @phones = Phone.where(id: params[:id])
    end

end
